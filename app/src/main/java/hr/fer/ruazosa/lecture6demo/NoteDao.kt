package hr.fer.ruazosa.lecture6demo
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NoteDao {
    @Query("SELECT * FROM note")
    fun getAll(): List<Note>

    @Query("SELECT count(*) FROM note")
    fun getCount(): Int

    @Insert
    fun insertAll(vararg note: Note)

    @Query("SELECT * from note where noteId = :noteId")
    fun getNoteById(noteId: Int): Note
}