package hr.fer.ruazosa.lecture6demo


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class NotesAdapter(notesViewModel: NotesViewModel): RecyclerView.Adapter<NotesAdapter.NotesViewHolder>() {

    var notesViewModel = notesViewModel

    class NotesViewHolder(noteView: View): RecyclerView.ViewHolder(noteView) {
        var noteDateTextView: TextView = noteView.findViewById(R.id.noteDateTextViewId)
        var noteTitleTextView: TextView = noteView.findViewById(R.id.noteTitleTextViewId)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val notesView = inflater.inflate(R.layout.note_cell, parent, false)
        return NotesViewHolder(notesView)
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        val note = notesViewModel.getNote(position)
        holder.noteDateTextView.setText(note.noteDate.toString())
        holder.noteTitleTextView.setText(note.noteTitle.toString())
    }

    override fun getItemCount(): Int {
        return notesViewModel.getNoteCount()
    }
}