package hr.fer.ruazosa.lecture6demo

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class NotesViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var noteRepository: NoteRepository
    private var repositoryInitialization: Job
    private var listOfNotes = listOf<Note>()
    var repositoryRefreshed = MutableLiveData<Boolean>()

    init {
        repositoryInitialization = viewModelScope.launch(Dispatchers.IO) {
            noteRepository = NoteRepository(application)
        }
    }

    fun refreshRepositoryState() {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryInitialization.join()
            listOfNotes = noteRepository.getNotes()
            repositoryRefreshed.postValue(true)
        }
    }

    fun saveNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepository.saveNote(note)
        }
    }

    fun getNote(noteAtIndex: Int): Note {
        return listOfNotes[noteAtIndex]
    }

    fun getNoteCount(): Int {
        return listOfNotes.size
    }
}