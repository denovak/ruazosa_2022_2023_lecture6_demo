package hr.fer.ruazosa.lecture6demo

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters


class NoteRepository(applicationContext:Application) {

    @Database(entities = [Note::class], version = 1)
    @TypeConverters(DateConverter::class)
    abstract class AppDatabase : RoomDatabase() {
        abstract fun noteDao(): NoteDao
    }

    private var database: AppDatabase = Room.databaseBuilder(
        applicationContext,
        AppDatabase::class.java, "notes-db"
    ).build()

    fun saveNote(note: Note) {
        database.noteDao().insertAll(note)
    }

    fun getNoteCount(): Int {
        return database.noteDao().getCount()
    }

    fun getNotes(): List<Note> {
        return database.noteDao().getAll()
    }
}