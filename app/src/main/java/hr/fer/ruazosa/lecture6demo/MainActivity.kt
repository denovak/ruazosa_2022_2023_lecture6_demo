package hr.fer.ruazosa.lecture6demo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.ruazosa.lecture6demo.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: NotesViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)).get(
            NotesViewModel::class.java)

        binding.notesListRecyclerViewId.layoutManager = LinearLayoutManager(applicationContext)
        val adapter = NotesAdapter(viewModel)
        binding.notesListRecyclerViewId.adapter = adapter

        val decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.divider)!!)
        binding.notesListRecyclerViewId.addItemDecoration(decorator)

        binding.floatingActionButton.setOnClickListener {
            val startNoteDetailsActivity = Intent(this, NoteDetailsActivity::class.java)
            startActivity(startNoteDetailsActivity)
        }

        viewModel.repositoryRefreshed.observe(this, Observer {
            if (it) {
                adapter.notifyDataSetChanged()
            }
        })

    }

    override fun onResume() {
        super.onResume()
        viewModel.refreshRepositoryState()
    }
}