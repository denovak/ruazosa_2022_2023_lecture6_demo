package hr.fer.ruazosa.lecture6demo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import java.util.Date

@Entity
data class Note (
    @PrimaryKey(autoGenerate = true) val noteId: Int,
    @TypeConverters(DateConverter::class)
    @ColumnInfo(name = "note_date") var noteDate: Date,
    @ColumnInfo(name = "note_title") var noteTitle: String,
    @ColumnInfo(name = "note_description") var noteDescription: String)