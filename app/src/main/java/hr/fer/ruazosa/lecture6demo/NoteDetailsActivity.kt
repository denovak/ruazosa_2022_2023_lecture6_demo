package hr.fer.ruazosa.lecture6demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import hr.fer.ruazosa.lecture6demo.databinding.ActivityNoteDetailsBinding
import java.util.Date

class NoteDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNoteDetailsBinding
    private lateinit var viewModel: NotesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNoteDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)).get(
            NotesViewModel::class.java)

        binding.saveNoteButtonId.setOnClickListener {
            val note = Note(noteDate = Date(), noteTitle = binding.noteDetailsTitleEditTextId.text.toString(),
                noteDescription = binding.noteDetailsNoteDescriptionEditTextId.toString(), noteId = 0)
            viewModel.saveNote(note)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.refreshRepositoryState()
    }
}